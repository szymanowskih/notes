﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notes.Migrations
{
    public partial class IdentityModelFix : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Base_AspNetUsers_IdentityUserId1",
                table: "Base");

            migrationBuilder.DropIndex(
                name: "IX_Base_IdentityUserId1",
                table: "Base");

            migrationBuilder.DropColumn(
                name: "IdentityUserId1",
                table: "Base");

            migrationBuilder.AlterColumn<string>(
                name: "IdentityUserId",
                table: "Base",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.CreateIndex(
                name: "IX_Base_IdentityUserId",
                table: "Base",
                column: "IdentityUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Base_AspNetUsers_IdentityUserId",
                table: "Base",
                column: "IdentityUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Base_AspNetUsers_IdentityUserId",
                table: "Base");

            migrationBuilder.DropIndex(
                name: "IX_Base_IdentityUserId",
                table: "Base");

            migrationBuilder.AlterColumn<int>(
                name: "IdentityUserId",
                table: "Base",
                type: "int",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IdentityUserId1",
                table: "Base",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Base_IdentityUserId1",
                table: "Base",
                column: "IdentityUserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Base_AspNetUsers_IdentityUserId1",
                table: "Base",
                column: "IdentityUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
