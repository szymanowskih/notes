﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Notes.Migrations
{
    public partial class IdentityModel : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "IdentityUserId",
                table: "Base",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IdentityUserId1",
                table: "Base",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Note_IdentityUserId",
                table: "Base",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Note_IdentityUserId1",
                table: "Base",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Base_IdentityUserId1",
                table: "Base",
                column: "IdentityUserId1");

            migrationBuilder.CreateIndex(
                name: "IX_Base_Note_IdentityUserId1",
                table: "Base",
                column: "Note_IdentityUserId1");

            migrationBuilder.AddForeignKey(
                name: "FK_Base_AspNetUsers_IdentityUserId1",
                table: "Base",
                column: "IdentityUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Base_AspNetUsers_Note_IdentityUserId1",
                table: "Base",
                column: "Note_IdentityUserId1",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Base_AspNetUsers_IdentityUserId1",
                table: "Base");

            migrationBuilder.DropForeignKey(
                name: "FK_Base_AspNetUsers_Note_IdentityUserId1",
                table: "Base");

            migrationBuilder.DropIndex(
                name: "IX_Base_IdentityUserId1",
                table: "Base");

            migrationBuilder.DropIndex(
                name: "IX_Base_Note_IdentityUserId1",
                table: "Base");

            migrationBuilder.DropColumn(
                name: "IdentityUserId",
                table: "Base");

            migrationBuilder.DropColumn(
                name: "IdentityUserId1",
                table: "Base");

            migrationBuilder.DropColumn(
                name: "Note_IdentityUserId",
                table: "Base");

            migrationBuilder.DropColumn(
                name: "Note_IdentityUserId1",
                table: "Base");
        }
    }
}
