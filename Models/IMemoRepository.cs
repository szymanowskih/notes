﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Notes.Models
{
    public interface IMemoRepository
    {
        IEnumerable<List> LoadAllLists();
        //IEnumerable<Base> LoadAllMemos();
        IEnumerable<Note> LoadAllNotes();
        IEnumerable<Item> LoadAllItems();
        //Note LoadNoteById(int id);
        Item LoadItemById(int id);
        Base LoadMemoById(int id);
        //List LoadListById(int id);
        void AddItem(Item item);
        void AddList(List list);
        //void AddImage(Image image);
        void AddNote(Note note);
        void EditList(int id, string title);
        void EditNote(int id, string text, string title);
        void EditItem(int id, string text);
        //void DeleteImage(Image image);
        //void DeleteList(List list);
        void Delete(Base memo);
        void DeleteItem(Item item);
        void UnCheckItem(int id);
        void CheckItem(int id);
    }
}
