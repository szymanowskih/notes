﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Notes.Models
{
    public class AppDbContext : IdentityDbContext<IdentityUser>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Note> Notes { get; set; }
        public DbSet<List> Lists { get; set; }
        public DbSet<Image> Images { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<Base> Bases { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            //modelBuilder.Entity<Note>().ToTable("Note");
            //modelBuilder.Entity<List>().ToTable("List");
            modelBuilder.Entity<Image>().ToTable("Image");
            modelBuilder.Entity<Item>().ToTable("Item");
            modelBuilder.Entity<Base>().ToTable("Base");

        }

    }
}
    