﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Notes.Models
{
    public abstract class Base
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Color { get; set; }
        public string IdentityUserId {get; set; }
        public virtual IdentityUser IdentityUser { get; set; }
    }
}
