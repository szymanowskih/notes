﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Notes.Models
{
    public class Item
    {
        public int Id { get; set; }
        public string Text { get; set; }
        public bool Checked { get; set; }
        public int ListId { get; set; }
        public virtual List List { get; set; }
    }
}
