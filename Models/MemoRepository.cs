﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Notes.Models
{
    public class MemoRepository : IMemoRepository

    {
        private readonly AppDbContext _appDbContext;

        public MemoRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Note> LoadAllNotes()
        {
            return _appDbContext.Notes;
        }

        public IEnumerable<List> LoadAllLists()
        {
            return _appDbContext.Lists;
        }

        public IEnumerable<Item> LoadAllItems()
        {
            return _appDbContext.Items;
        }

        public Base LoadMemoById(int id)
        {
            return _appDbContext.Bases.FirstOrDefault(m => m.Id == id);
        }

        public Item LoadItemById(int id)
        {
            return _appDbContext.Items.FirstOrDefault(m => m.Id == id);
        }

        public void AddItem(Item item)
        {
            _appDbContext.Items.Add(item);
            _appDbContext.SaveChanges();
        }

        public void AddList(List list)
        {
            _appDbContext.Lists.Add(list);
            _appDbContext.SaveChanges();
        }

        public void AddNote(Note note)
        {
            _appDbContext.Notes.Add(note);
            _appDbContext.SaveChanges();
        }

        //public void DeleteList(List list)
        //{
        //    _appDbContext.Lists.Remove(list);
        //    _appDbContext.SaveChanges();
        //}

        public void Delete(Base memo)
        {
            _appDbContext.Bases.Remove(memo);
            _appDbContext.SaveChanges();
        }

        public void DeleteItem(Item item)
        {
            _appDbContext.Items.Remove(item);
            _appDbContext.SaveChanges();
        }

        public void EditList(int id, string title)
        {
            var list = _appDbContext.Lists.FirstOrDefault(n => n.Id == id);
            list.Title = title;
            _appDbContext.SaveChanges();
        }

        public void EditNote(int id, string text, string title)
        {
            var note = _appDbContext.Notes.FirstOrDefault(n => n.Id == id);
            note.Text = text;
            note.Title = title;
            _appDbContext.SaveChanges();
        }

        public void EditItem(int id, string text)
        {
            Item item = _appDbContext.Items.FirstOrDefault(i => i.Id == id);
            item.Text = text;
            _appDbContext.SaveChanges();
        }

        public void UnCheckItem(int id)
        {
            Item item = _appDbContext.Items.FirstOrDefault(i => i.Id == id);
            item.Checked = false;
            _appDbContext.SaveChanges();
        }

        public void CheckItem(int id)
        {
            Item item = _appDbContext.Items.FirstOrDefault(i => i.Id == id);
            item.Checked = true;
            _appDbContext.SaveChanges();
        }



        //public IEnumerable<Base> LoadAllMemos()
        //{
        //    return _appDbContext.Bases;
        //}



        //public Note LoadNoteById(int id)
        //{
        //    return _appDbContext.Notes.FirstOrDefault(m => m.Id == id);
        //}

        //public List LoadListById(int id)
        //{
        //    return _appDbContext.Lists.FirstOrDefault(m => m.Id == id);
        //}




    }
}
