﻿using Notes.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Notes.ViewModels
{
    public class MemoVM
    {
        public List<Note> Notes { get; set; }
        public List<List> Lists { get; set; }
        public List<Item> Items { get; set; }
        public List<Image> Images { get; set; }
    }
}
