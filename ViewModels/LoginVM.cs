﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Notes.ViewModels
{
    public class LoginVM
    {
        
        [Display (Name = "name")]
        public string UserName { get; set; }
        [Required]
        [Display(Name = "e-mail address")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [Display (Name = "password")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
