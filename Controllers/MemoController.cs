﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Migrations.Operations;
using Notes.Models;
using Notes.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Notes.Controllers
{
    [Authorize]
    public class MemoController : Controller
    {
        private readonly IMemoRepository _memoRepository;

        public MemoController(IMemoRepository memoRepository)
        {
            _memoRepository = memoRepository;
        }

        public IActionResult Index()
        {
            var notes = _memoRepository.LoadAllNotes();
            var lists = _memoRepository.LoadAllLists();
            var items = _memoRepository.LoadAllItems();
            var memoVM = new MemoVM
            {
                Notes = notes.ToList(),
                Lists = lists.ToList(),
                Items = items.ToList()
            };

            return View("Index", memoVM);
        }

        public IActionResult CreateList(string title)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            List list = new List { Title = title, IdentityUserId = userId };
            _memoRepository.AddList(list);

            return Json(new
            {
                success = true,
                data = list.Id
            });
        }

        public IActionResult CreateNote(string title, string text)
        {
            var userId = User.FindFirstValue(ClaimTypes.NameIdentifier);
            Note note = new Note { Title = title, Text = text, IdentityUserId = userId };
            _memoRepository.AddNote(note);

            return Json(new
            {
                success = true,
                data = note.Id
            });
        }

        public IActionResult CreateItem(int listId, string itemtext)
        {
            Item item = new Item { ListId = listId, Text = itemtext, Checked = false };
            _memoRepository.AddItem(item);

            return Json(new
                {
                success = true,
                    data = item.Id
                  
            });
        }

        public IActionResult EditNote(int id, string text, string title)
        {
            _memoRepository.EditNote(id, text, title);

            return Json(new
            {
                success = true
            });

        }

        public IActionResult EditList(int id, string title)
        {
            _memoRepository.EditList(id, title);

            return Json(new
            {
                success = true
            });

        }

        public IActionResult EditItem(int id, string itemtext)
        {
            _memoRepository.EditItem(id, itemtext);

            return Json(
                new
                {
                    success = true
                });
        }

        public IActionResult Delete(int id)
        {
            var memo = _memoRepository.LoadMemoById(id);
            _memoRepository.Delete(memo);

            return Json(new
            {
                success = true,
            });

        }
        public IActionResult DeleteItem(int id)
        {
            var item = _memoRepository.LoadItemById(id);
            _memoRepository.DeleteItem(item);

            return Json(new
            {
                success = true,
            });
                
        }

        public IActionResult UnCheck(int id)
        {
            _memoRepository.UnCheckItem(id);

            return Json(new
            {
                success = true,
            });
            
        }

        public IActionResult Check(int id)
        {
            _memoRepository.CheckItem(id);

            return Json(new
            {
                success = true,
            });
        }

    }
}
